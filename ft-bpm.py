#!/usr/bin/env python

min_hz = 25
max_hz = 400
min_speed = 1
max_speed = 31
min_tempo = 32
max_tempo = 255
min_row_highlight = 1
max_row_highlight = 32


def calculate(desired_bpm, divergence, beat_divider1, beat_divider2=-1):
    results = []
    for hz in range(min_hz, max_hz):
        tempo = 2.5 * hz
        if tempo % 1 == 0:
            for speed in range(min_speed, max_speed):
                for row_highlight in range(min_row_highlight, max_row_highlight):
                    c1 = is_valid_hz(hz)
                    c2 = is_valid_speed(speed)
                    c3 = is_valid_tempo(tempo)
                    c4 = is_valid_row_highlight(row_highlight, beat_divider1, beat_divider2, speed)
                    possible_bpm = tempo * 24 / (speed * row_highlight)
                    c5 = possible_bpm - divergence <= desired_bpm <= possible_bpm + divergence
                    if c1 and c2 and c3 and c4 and c5:
                        result = {
                            'hz': hz,
                            'speed': speed,
                            'tempo': tempo,
                            'row_highlight': row_highlight,
                            'possible_bpm': possible_bpm,
                        }
                        results.append(result)
    print(*results if len(results) else 'No values found', sep='\n')


def is_valid_hz(hz):
    c1 = min_hz <= hz <= max_hz
    c2 = hz % 1 == 0
    return c1 and c2


def is_valid_speed(speed):
    c1 = min_speed <= speed <= max_speed
    c2 = speed % 1 == 0
    return c1 and c2


def is_valid_tempo(tempo):
    return min_tempo <= tempo <= max_tempo


def is_valid_row_highlight(row_highlight, beat_divider1, beat_divider2, speed):
    c1 = min_row_highlight <= row_highlight <= max_row_highlight
    c2 = row_highlight >= beat_divider1
    c3 = row_highlight % beat_divider1 == 0
    c4 = row_highlight > beat_divider2
    c5 = row_highlight * speed % beat_divider2 == 0
    return c1 and c2 and c3 and c4 and c5


desired_bpm = input('Enter desired BPM: ')
divergence = input('Enter divergence (how far the found BPM values may deviate from the desired BPM, 0 for only exact matches): ')
beat_divider1 = input('Enter beat divider 1 (minimum row highlight 1 you want to use in FamiTracker): ')
beat_divider2 = input('Enter beat divider 2 (optional, another number a beat should be divisible by, e.g. 3 for triplets): ') or "-1"
calculate(int(desired_bpm), int(divergence), int(beat_divider1), int(beat_divider2))
