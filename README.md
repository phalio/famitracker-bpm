# famitracker-bpm

A Python script that provides you with a range of possible value sets to achieve your desired BPM in FamiTracker.

The original version was coded by my friend Amarizo in April 2020 according to my findings of how song speed in FamiTracker works. Since then I’ve been using it for every single FamiTracker project, it’s really helpful and will be expanded in the future.

## Features

Currently, the script can be used to find the nearest possible BPM with an equal amount of ticks per row. It includes results for modified engine speeds (Hz) since default engine speeds allow for very few tempi with equal ticks per row, but you can just ignore results with non-standard Hz values if you don’t want to change the engine speed. I personally allow myself the freedom of changing the engine speed since more possible tempi as well as even note lengths and ticks per row are more important to me than being absolutely true to original hardware conditions. Dedicated features for default engine speeds, fractional/uneven ticks per row like 0CC-FamiTracker’s groove settings and more are planned.

## Usage

Upon running the script, you will be asked for the desired BPM, the allowed divergence (how far the found BPM values may deviate from the desired BPM, 0 for no deviation), beat divider 1 (the minimum row highlight 1 you want to use in FamiTracker) and an optional second beat divider for when you want to make sure that the beat is evenly divisible by another number, e.g. by 3 for equal triplets in four-four time. After providing the inputs, you will be given a list of value sets to use in FamiTracker for possible BPM values it has found. If it finds no results or none that satisfy you, run it again with a slightly increased divergance value. If it finds too many, decrease it.

After finding a good set of values, you have to put them into FamiTracker. The engine speed in Hz can be changed in the top menu bar by going to `Module` -> `Engine Speed` -> `Custom`. The slider is a little small and many values are skipped but you can click on the bar once and then use the left and right arrow keys to set an exact value. Speed and tempo are of course the fields in the top left corner. Row highlight is "Row highlight, 1st" at the top. The 2nd row highlight is entirely cosmetic. After inputting these values, the BPM value displayed at the bottom right should correspond to one of the value set you chose.

## How BPM Calculation Works

The Secret Formula is the following:

`BPM = Tempo × 24 ÷ (Speed × RowHighlight1)`

These values have the following restrictions:

**Speed (ticks per row):** `≥ 1`, `≤ 31`, `integer`

**RowHighlight1 (rows per beat):** `≥ 1`, `≤ 32`, `integer`

**EngineSpeed (ticks per second in Hz):** `≥ 25` (`≥ 16` in 0CC-FamiTracker), `≤ 400`, `integer`, tied to tempo

**Tempo (pointless number to make things more complicated and limited):** `≥ 32`, `≤ 255`, `integer`, must always be `2.5 × EngineSpeed`

Engine speed and tempo are tied together. If tempo is not `2.5 × EngineSpeed`, each row will have a different and unpredictable amount of ticks instead of the value of speed. It’s better to use other methods if an uneven amount of ticks per row and therefore unequal length per note is acceptable, like using Fxx. This interdependence limits valid value sets a lot since both of them are also constrained by the other one’s restrictions. If `2.5 × EngineSpeed` does not result in a valid tempo value (within possible range, integer), that engine speed also cannot be used. The script takes care of all of this, along with the option to make sure that the amount of ticks per beat is also divisible by another number for tuplets of equal integer tick length.
